<?php

use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

$routes = new RouteCollection();
$routes->add('add_note', new Route('/add', ['controller' => 'IndexController', 'method' => 'addNote']));
$routes->add('index', new Route('/', ['controller' => 'IndexController', 'method' => 'index']));
