# Startup

## Installation

* Init composer dependencies

```
composer install
```

* Rename file **.env.config** to **.env**
* Set .env configuration params like 
```
SOME_VARIABLE="value"
```