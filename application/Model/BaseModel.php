<?php

namespace App\Model;

abstract class BaseModel
{
    public const FIELDS = [];
    public const TABLE_NAME = '';

    protected ?int $id;
    protected ?\DateTime $createdAt;
    protected ?\DateTime $updatedAt;

    public function __construct()
    {
        $this->id = null;
        $this->createdAt = null;
        $this->updatedAt = null;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return BaseModel
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Check if model new or not
     *
     * @return bool
     */
    public function isNew(): bool
    {
        return $this->getId() === null;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime|null
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return Note
     */
    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @param \DateTime $updatedAt
     * @return Note
     */
    public function setUpdatedAt(\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        $methodName = 'get' . ucfirst($name);

        if(method_exists($this, $methodName)) {
            return $this->$methodName();
        }

        throw new \RuntimeException("Cannot get model param '$name'. Get method does not exists");
    }
}