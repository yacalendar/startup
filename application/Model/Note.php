<?php

namespace App\Model;

class Note extends BaseModel
{
    public const FIELDS = [
        'forDate',
        'note',
    ];
    public const TABLE_NAME = 'note';

    protected string $forDate;
    protected string $note;

    /**
     * @return string
     */
    public function getForDate(): string
    {
        return $this->forDate;
    }

    /**
     * @return string
     */
    public function getNote(): string
    {
        return $this->note;
    }

    /**
     * @param \DateTime $forDate
     * @return Note
     */
    public function setForDate(\DateTime $forDate): self
    {
        $this->forDate = $forDate->format('Y-m-d');

        return $this;
    }

    /**
     * @param string $note
     * @return Note
     */
    public function setNote(string $note): self
    {
        $this->note = $note;

        return $this;
    }
}