<?php

namespace App\DB;

use App\Model\BaseModel;

class EntityManager
{
    /**
     * @var BaseModel[]
     */
    private array $dataForUpdate = [];

    /**
     * Add data for insert/update
     *
     * @param BaseModel $model
     */
    public function persist(BaseModel $model)
    {
        $this->dataForUpdate[] = $model;
    }

    /**
     * Store data in database
     */
    public function flush()
    {
        $dbConnection = DB::connect();

        $dbConnection->beginTransaction();

        foreach ($this->dataForUpdate as $model) {
            $methodName = $model->isNew() ? 'insert' : 'update';

            $dbConnection->$methodName($model);
        }

        $this->dataForUpdate = [];

        $dbConnection->commit();
    }
}