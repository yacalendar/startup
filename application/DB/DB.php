<?php

namespace App\DB;

use App\Model\BaseModel;
use PDO;

class DB
{
    private PDO $connection;

    /**
     * @param PDO $connection
     */
    private function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Get database connection
     *
     * @return DB
     */
    public static function connect(): DB
    {
        return new self(new PDO(
            'mysql:dbname=' . DB_DATABASE . ';host=' . DB_HOST,
            DB_USER,
            DB_PASSWORD,
            [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'"]
        ));
    }

    /**
     * Insert model into database
     *
     * @param BaseModel $model
     * @noinspection PhpUnused
     */
    public function insert(BaseModel $model)
    {
        $this->validateModel($model);

        $data = [];

        foreach ($model::FIELDS as $field) {
            $data[$this->convertCamelcaseToSnakeCase($field)] = $model->$field;
        }

        $now = new \DateTime();

        $data[$this->convertCamelcaseToSnakeCase('createdAt')] = $now->format('Y-m-d H:i:s');
        $data[$this->convertCamelcaseToSnakeCase('updatedAt')] = $now->format('Y-m-d H:i:s');

        $query = 'INSERT INTO ' . $model::TABLE_NAME . ' (' . implode(',', array_keys($data)) .
            ') VALUES (' . implode(',', array_map(static fn(string $field) => ":$field", array_keys($data))) . ')';
        $statement = $this->connection->prepare($query);

        foreach ($data as $field => $value) {
            $statement->bindValue(":$field", $value);
        }

        $statement->execute();

        $model->setId($this->connection->lastInsertId())->setCreatedAt($now)->setUpdatedAt($now);
    }

    /**
     * @param BaseModel $model
     */
    private function validateModel(BaseModel $model)
    {
        if(empty($model::FIELDS)) {
            throw new \LogicException("Cannot find fields for model '" . get_class($model) . "'");
        }

        if(empty($model::TABLE_NAME)) {
            throw new \LogicException("Model '" . get_class($model) . "' does not have table name");
        }
    }

    /**
     * Convert from "exampleString" to "example_string"
     *
     * @param string $string
     * @return string
     */
    private function convertCamelcaseToSnakeCase(string $string): string
    {
        return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $string));
    }

    /**
     * Update model in database
     *
     * @param BaseModel $model
     * @noinspection PhpUnused
     */
    public function update(BaseModel $model)
    {
        $this->validateModel($model);

        $query = 'UPDATE ' . $model::TABLE_NAME . ' SET ';

        foreach ($model::FIELDS as $field) {
            $fieldName = $this->convertCamelcaseToSnakeCase($field);

            if($field === 'createdAt' || $field === 'updatedAt') {
                $query .= $fieldName . ' = ' . $model->$field->format('Y-m-d H:i:s');

                continue;
            }

            $query .= $fieldName . ' = ' . $model->$field;
        }

        $query .= ' WHERE id = ? LIMIT 1';

        $statement = $this->connection->prepare($query);
        $statement->execute([$model->getId()]);
    }

    /**
     * Start transaction
     */
    public function beginTransaction()
    {
        $this->connection->beginTransaction();
    }

    /**
     * Finish transaction
     */
    public function commit()
    {
        $this->connection->commit();
    }

    /**
     * Rollback transaction
     */
    public function rollback()
    {
        $this->connection->rollBack();
    }
}