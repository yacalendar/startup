<?php

namespace App\Controller;

use App\DB\EntityManager;
use App\Model\Note;
use Symfony\Component\Routing\RouteCollection;

class IndexController
{
    public function index(RouteCollection $routes)
    {
        require_once VIEW_ROOT . 'index.php';
    }

    /**
     * Example how we add notes
     *
     * @param RouteCollection $routes
     * @throws \Exception
     */
    public function addNote(RouteCollection $routes)
    {
        $newNote = (new Note())->setForDate(new \DateTime($_POST['for_date']))->setNote($_POST['note']);

        $manager = new EntityManager();

        $manager->persist($newNote);

        $manager->flush();
    }
}