<?php

use App\Router;
use Dotenv\Dotenv;

require_once '../vendor/autoload.php';

Dotenv::createImmutable(dirname(__FILE__, 2))->load();

require_once '../config/main.php';
require_once '../route/web.php';

$router = new Router();
/** @noinspection PhpUndefinedVariableInspection */
$router($routes);