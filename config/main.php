<?php /** @noinspection PhpDefineCanBeReplacedWithConstInspection */

define('PROJECT_ROOT', dirname(__FILE__, 2) . '/');
define('VIEW_ROOT', PROJECT_ROOT . 'view/');

require_once 'db.php';